<?php

if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}

include '../controller/database.php';

function addCustomer() {
    $conn = getDb();

    $name = $_REQUEST['name'];
    $surname = $_REQUEST['surname'];
    $address = $_REQUEST['address'];
    $meterNumber = $_REQUEST['meter'];
    $email = $_REQUEST['email'];
    $postcode = $_REQUEST['postcode'];
    $authorisation = $_REQUEST['authorisation'];
    $password = md5($_REQUEST['password']);


    $sql = $conn->prepare("INSERT INTO customer (name, surname, address, meter_number, email, postcode, authorisation, password)
    VALUES (:name,:surname,:address,:meterNumber,:email,:postcode,:authorisation, :password)");
    $sql->bindValue(":name", $name);
    $sql->bindValue(":surname", $surname);
    $sql->bindValue(":address", $address);
    $sql->bindValue(":meterNumber", $meterNumber);
    $sql->bindValue(":email", $email);
    $sql->bindValue(":postcode", $postcode);
    $sql->bindValue(":authorisation", $authorisation);
    $sql->bindValue(":password", $password);

    try {
        $sql->execute();
        header('Location: ../index.php');
    } catch (Exception $e) {
        echo '<script language="javascript">';
        echo 'alert("Failed to add customer")';
        echo '</script>';
        echo $e->getMessage();
    }
}

?>