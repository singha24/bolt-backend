<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';

function newTarrif() {
    $conn = getDb();

    if (isset($_POST['meter_number'])) {
        $meter_number = $_POST['meter'];
        $tarrif_id = $_POST['tarrif_id'];
        

        $sql = $conn->prepare("SELECT customer.name, customer.surname, customer.meter_number, customer.postcode, meter_association.*, tarrif.id, tarrif.tarrif_name WHERE "
                . "customer.meter_number=:meter_number AND "
                . "customer.meter_number=meter_association.meter_number AND "
                . "meter_association.tarrif_id=tarrif.tarrif_id");
        $sql->bindValue(":meter_number", $meter_number);

        try {
            $sql->execute();
            // set the resulting array to associative
            $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
            $rows = $sql->fetchAll(); // holds array containing each row
            return $rows;
        } catch (Exception $e) {
            echo '<script language="javascript">';
            echo 'alert("Failed to find customer")';
            echo '</script>';
            echo $e->getMessage();
        }
    }
}

function getTarrifs() {
    $conn = getDb();
    $sql = $conn->prepare("SELECT tarrif.tarrif_name, tarrif.tarrif_id FROM tarrif");

    try {
        $sql->execute();
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        foreach ($rows as $row) {
            echo'<option selected="selected" value="' . $row['tarrif_id'] . '">' . $row['tarrif_name'] . '</option>';
        }
    } catch (Exception $e) {
        echo '<script language="javascript">';
        echo 'alert("Failed to find customer")';
        echo '</script>';
        echo $e->getMessage();
    }
}

?>