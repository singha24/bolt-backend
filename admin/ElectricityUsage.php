<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sum of Usage</title>
        <link rel="stylesheet" type="text/css" href="../css/view_customers.css">
    </head>
    <body>

        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <!--<th><a href="#">No.</a></th>
                        <th><a href="#">Meter Number</a></th>
                        <th><a href="#">Date</a></th>
                        <th><a href="#">Time</a></th>
                        <th><a href="#">Usage ELECTRICITY</a></th>
                        <th><a href="#">Electricity Price</a></th>
                        <th><a href="#">TCR</a></th>
                        <th><a href="#">Electricity SC</a></th>
                        <th><a href="#">Cancellation Fee</a></th>-->

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL

                    $meter_number = 3476587346836;
                    $start = "2015-04-23";

                    $endP1 = date("Y-m-d");

                    $dd = DateTime::createFromFormat("Y-m-d", $endP1);
                    $endDayMinus1 = $dd->format("d") -1;

                    $mm = DateTime::createFromFormat("Y-m-d", $endP1);
                    $endMonth = $mm->format("m");

                    $yy = DateTime::createFromFormat("Y-m-d", $endP1);
                    $endYear = $yy->format("y");

                    $endOfMonth = strtotime($endYear . '-' . $endMonth . '-' . $endDayMinus1);
                    $end = gmdate("Y-m-d", $endOfMonth);

                    //$endP1 = strtotime($endP1 . ' + 1 day');
                    //$endP1 = gmdate("Y-m-d", $endP1); //convert to valid format

                    date_default_timezone_set("Europe/London");
                    $time = date("H:i");

                    echo $time;
                    echo '<h1>Electricity Usage From: ' . $start . ' To: ' . $endP1 . '</h1>';
                    echo '<hr>';

                    $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage, tarrif.* FROM "
                            . "customer, meter_association, historic_usage_elec, tarrif WHERE "
                            . "customer.meter_number=:meter_number AND "
                            . "meter_association.meter_number=customer.meter_number AND "
                            . "meter_association.tarrif_id=tarrif.tarrif_id AND "
                            . "meter_association.meter_number=historic_usage_elec.meter_number AND "
                            . "historic_usage_elec.day BETWEEN :start AND :end");

                    $sql->bindValue(":meter_number", $meter_number);
                    $sql->bindValue(":start", $start);
                    $sql->bindValue(":end", $end);

                    $sql->execute(); // runs SQL statement
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row

                    if ($result != false) {
                        $counter = 1;
                        $elec_usage = 0;
                        $elec_sc = 0;
                        $elec_price = 0;
                        foreach ($rows as $row) {

                            //echo "<tr>";
                            //echo "<td  class='overflow-ellipsis'> " . $counter . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['meter_number'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['day'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['elec_usage'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['electricity_price'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['tcr'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['electricity_standing_charge'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['cancellation_fee'] . "</td>";

                            $counter++;
                            $elec_usage += $row['elec_usage'];
                            $elec_sc = $row['electricity_standing_charge'];
                            $elec_price = $row['electricity_price'];
                            //echo "</tr>";
                        }

                        echo '<hr/>';

                        $sql2 = $conn->prepare("SELECT historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage FROM "
                                . "historic_usage_elec WHERE "
                                . "historic_usage_elec.day = :endP AND "
                                . "historic_usage_elec.time <= :time");
                        $sql2->bindValue(":time", $time);
                        $sql2->bindValue(":endP", $endP1);
                        $sql2->execute();
                        $result = $sql2->setFetchMode(PDO::FETCH_ASSOC);
                        $rows = $sql2->fetchAll(); // holds array containing each row

                        foreach ($rows as $row) {
                            //echo "<tr>";
                            //echo "<td  class='overflow-ellipsis'> " . $counter . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . '-----' . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['day'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['elec_usage'] . "</td>";
                            //echo "<tr/>";
                            $elec_usage += $row['elec_usage'];
                            $counter++;
                        }
                        //Gets data for previous month
                        $previous = strtotime($start . ' - 1 month');

                        $startOfPreviousMonth = gmdate("Y-m-d", $previous); //previous month

                        $d = DateTime::createFromFormat("Y-m-d", $endP1);
                        $endDay = $d->format("d");
                        $day = $d->format("d") - 1;

                        $m = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonth);
                        $month = $m->format("m");

                        $y = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonth);
                        $year = $y->format("y");



                        $endOfPreviousMonth = strtotime($year . '-' . $month . '-' . $day);
                        $endOfPreviousMonthMinus1 = gmdate("Y-m-d", $endOfPreviousMonth);

                        $aa = strtotime($year . '-' . $month . '-' . $endDay);
                        $actualEndOfPreviousMonth = gmdate("Y-m-d", $aa);

                        echo $startOfPreviousMonth . ' ' . $actualEndOfPreviousMonth;

                        $usagePreviousMonth = 0;

                        $sql3 = $conn->prepare("SELECT * FROM historic_usage_elec WHERE "
                                . "historic_usage_elec.meter_number=:meter_number AND "
                                . "historic_usage_elec.day BETWEEN :start AND :end");

                        $sql3->bindValue(":meter_number", $meter_number);
                        $sql3->bindValue(":start", $startOfPreviousMonth);
                        $sql3->bindValue(":end", $endOfPreviousMonthMinus1);

                        $sql3->execute(); // runs SQL statement
                        // set the resulting array to associative
                        $result = $sql3->setFetchMode(PDO::FETCH_ASSOC);
                        $rows = $sql3->fetchAll(); // holds array containing each row

                        foreach ($rows as $row) {
                            echo "<tr>";
                            echo "<td  class='overflow-ellipsis'> " . $counter . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . '-----' . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['day'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['elec_usage'] . "</td>";
                            $usagePreviousMonth += $row['elec_usage'];
                            $counter++;
                            echo "<td  class='overflow-ellipsis'> " . $usagePreviousMonth . "</td>";
                            echo "<tr/>";
                        }


                        //gets time and usage for previous month
                        $sql4 = $conn->prepare("SELECT historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage FROM "
                                . "historic_usage_elec WHERE "
                                . "historic_usage_elec.day = :endP AND "
                                . "historic_usage_elec.time <= :time");

                        $sql4->bindValue(":time", $time);
                        $sql4->bindValue(":endP", $actualEndOfPreviousMonth);
                        $sql4->execute();
                        $result = $sql4->setFetchMode(PDO::FETCH_ASSOC);
                        $rows = $sql4->fetchAll(); // holds array containing each row
                        foreach ($rows as $row) {
                            echo "<tr>";
                            echo "<td  class='overflow-ellipsis'> " . $counter . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . '-----' . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['day'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['elec_usage'] . "</td>";
                            $usagePreviousMonth += $row['elec_usage'];
                            $counter++;
                            echo "<td  class='overflow-ellipsis'> " . $usagePreviousMonth . "</td>";
                            echo "<tr/>";
                        }


                        //calculations

                        echo '<pre>';
                        echo '<hr/>';

                        $diff = abs(strtotime($endP1) - strtotime($start));

                        $years = floor($diff / (365 * 60 * 60 * 24));
                        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24)) + 1;
                        printf("%d years, %d months, %d days\n", $years, $months, $days);


                        $total = ($days * $elec_sc) + ($elec_usage * $elec_price); //1

                        echo 'Total: £' . $total . ' ';
                        echo 'Elec Usage This Month: ' . $elec_usage . 'kWh || ';
                        echo 'Usage Previous Month ' . $usagePreviousMonth . 'kWh ';

                        echo '<pre/>';
                    } else {
                        echo "<tr>";
                        echo "<td>" . 'Nothing to Display' . "</td>";
                        echo "</tr>";
                    }
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
    </body>
</html>
