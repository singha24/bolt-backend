<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sum of Usage</title>
        <link rel="stylesheet" type="text/css" href="../css/view_customers.css">
    </head>
    <body>
        
        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">Meter Number</a></th>
                        <th><a href="#">Date</a></th>
                        <th><a href="#">Time</a></th>
                        <th><a href="#">Usage GAS</a></th>
                        <th><a href="#">Gas Price</a></th>
                        <th><a href="#">TCR</a></th>
                        <th><a href="#">Gas SC</a></th>
                        <th><a href="#">Cancellation Fee</a></th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL
                    $start = "2009-01-1";
                    $end = "2009-07-21";
                    
                    echo '<h1>Gas Usage From: ' . $start . ' To: '. $end .'</h1>';
                    echo '<hr>';
                    
                    $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage, tarrif.* FROM "
                            . "customer, meter_association, historic_usage_gas, tarrif WHERE "
                            . "customer.meter_number=3476587346836 AND "
                            . "meter_association.meter_number=customer.meter_number AND "
                            . "meter_association.tarrif_id=tarrif.tarrif_id AND "
                            . "meter_association.meter_number=historic_usage_gas.meter_number AND "
                            . "historic_usage_gas.day BETWEEN :start AND :end");
                    $sql->bindValue(":start", $start);
                    $sql->bindValue(":end", $end);
                    $sql->execute(); // runs SQL statement
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row
                    
                    if ($result != false) {
                        foreach ($rows as $row) {

                            echo "<tr>";
                            echo "<td  class='overflow-ellipsis'> " . $row['meter_number'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['day'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['gas_usage'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['gas_price'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['tcr'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['gas_standing_charge'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['cancellation_fee'] . "</td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr>";
                            echo "<td>" . 'Nothing to Display' . "</td>";
                            echo "</tr>";
                    }
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
    </body>
</html>
