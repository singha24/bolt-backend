<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../DAO/ProcessNewCustomer.php';
if (isset($_REQUEST['function']) && $_REQUEST['function'] = "addcustomer") {
    addCustomer();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>New Customer</title>
        <link rel="stylesheet" type="text/css" href="../css/newbook.css">
    </head>
    <body>
        <h1> Add new Customer </h1>
        <hr>
        <div class="newCust">
            <form id="new_customer" method="post" action="">
                Name
                <input id="name" type="textarea" autofocus="true" name="name" maxlength="20" required>
                <br/>

                Suraname: 
                <input id="surname" type="textarea" name="surname" maxlength="20" required>
                <br/>

                Address:
                <input id="address" type="textarea" name="address" maxlength="50" required>
                <br/>
                
                Postcode: 
                <input id="postcode" type="text" name="postcode" maxlength="7" required>
                <br/>
                
                Email Address: 
                <input id="email" type="email" name="email" maxlength="50" required>
                <br/>
                
                Authorization (yes/no): 
                <input id="authorisation" type="text" name="authorisation" maxlength="3" required>
                <br/>

                Meter Number: 
                <input id="meter_number" type="text" name="meter" maxlength="13" required>
                <br/>

                Password: 
                <input id="password" type="password" name="password" maxlength="20" required>
                <br/>
                
                <input type='hidden' name='function' value="addcustomer" />
                <input type="submit" value="Submit" id="submitForm" />
            </form>
        </div>
    </body>
</html>
