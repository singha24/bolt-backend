<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sum of Usage</title>
        <link rel="stylesheet" type="text/css" href="../css/view_customers.css">
    </head>
    <body>

        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">Meter Number</a></th>
                        <th><a href="#">Date</a></th>
                        <th><a href="#">Time</a></th>
                        <th><a href="#">Usage ELECTRICITY</a></th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL

                    $meter_number = 3476587346836;
                    $start = "2015-01-01";
                    date_default_timezone_set("Europe/London");
                    $time = date("H:i");
                    echo $time;
                    echo '<h1>Electricity Usage: ' . $start . '</h1>';
                    echo '<hr>';

                    $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage FROM "
                            . "customer, meter_association, historic_usage_elec WHERE "
                            . "customer.meter_number=:meter_number AND "
                            . "meter_association.meter_number=customer.meter_number AND "
                            . "meter_association.meter_number=historic_usage_elec.meter_number AND "
                            . "historic_usage_elec.day=:start");

                    $sql->bindValue(":meter_number", $meter_number);
                    $sql->bindValue(":start", $start);

                    $sql->execute(); // runs SQL statement
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row

                    if ($result != false) {
                        foreach ($rows as $row) {

                            echo "<tr>";
                            echo "<td  class='overflow-ellipsis'> " . $row['meter_number'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['day'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['elec_usage'] . "</td>";
                            echo "</tr>";
                        }
                        echo '<hr/>';
                    } else {
                        echo "<tr>";
                        echo "<td>" . 'Nothing to Display' . "</td>";
                        echo "</tr>";
                    }
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
    </body>
</html>
