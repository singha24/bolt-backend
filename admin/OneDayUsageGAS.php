<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sum of Usage</title>
        <link rel="stylesheet" type="text/css" href="../css/view_customers.css">
    </head>
    <body>

        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">Meter Number</a></th>
                        <th><a href="#">Date</a></th>
                        <th><a href="#">Time</a></th>
                        <th><a href="#">Usage ELECTRICITY</a></th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL

                    $meter_number = 3476587346836;
                    $start = "2015-03-01";
                    date_default_timezone_set("Europe/London");
                    $time = date("H:i");
                    echo $time;
                    echo '<h1>Gas Usage: ' . $start . '</h1>';
                    echo '<hr>';

                    $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage FROM "
                            . "customer, meter_association, historic_usage_gas WHERE "
                            . "customer.meter_number=:meter_number AND "
                            . "meter_association.meter_number=customer.meter_number AND "
                            . "meter_association.meter_number=historic_usage_gas.meter_number AND "
                            . "historic_usage_gas.day=:start");

                    $sql->bindValue(":meter_number", $meter_number);
                    $sql->bindValue(":start", $start);

                    $sql->execute(); // runs SQL statement
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row

                    $total_gas_usage = 0;
                    $i = 0;
                    $counter = 1;
                    $endOfDayUsage = 0;
                    foreach ($rows as $row) {
                        echo "<tr>";
                        echo "<td  class='overflow-ellipsis'> " . $row['day'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['gas_usage'] . "</td>";
                        $endOfDayUsage += $row['gas_usage'];
                        echo "<td  class='overflow-ellipsis'> " . $endOfDayUsage . "</td>";
                        if ($counter % 48 == 0) {
                            echo "<td  class='overflow-ellipsis'> " . $endOfDayUsage . "</td>";
                            $endOfDayUsage = 0;
                            $counter = 0;
                        }
                        $total_gas_usage += $row['gas_usage']; //total
                        echo "</tr>";

                        $counter++;
                        $i++;
                    }
                    echo '<hr/>';
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
    </body>
</html>
