<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';
if (isset($_REQUEST['verify']) && $_REQUEST['verify'] = "Verify") {
    verify();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Tarrifs</title>
        <link rel="stylesheet" type="text/css" href="../css/view_customers.css">
    </head>
    <body>
        <h1>Tarrifs</h1>
        <hr>
        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">Tarrif ID</a></th>
                        <th><a href="#">Gas Price (p/kWh)</a></th>
                        <th><a href="#">Gas Standing Charge (p/kWh)</a></th>
                        <th><a href="#">Electricity Price (p/kWh)</a></th>
                        <th><a href="#">Electricity Standing Charge (p/kWh)</a></th>
                        <th><a href="#">Length (months)</a></th>  <!-- tarrif type FIXED / VARIABLE -->
                        <th><a href="#">Tarrif Name</a></th>
                        <th><a href="#">Supplier</a></th>
                        <th><a href="#">GAS TCR (p/kWh)</a></th>
                        <th><a href="#">ELECTRICITY TCR (p/kWh)</a></th>
                        <th><a href="#">Energy Type</a></th> <!--gas, electicity, both -->
                        <th><a href="#">Cancellation Fee (£)</a></th>
                        <!--<th><a href="#">Payment Method</a></th> <!--monthly direct debit || payg || eco7 or not -->
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL

                    $sql = $conn->prepare("SELECT tarrif.*, supplier.supplier, supplier.type, supplier_association.* FROM tarrif, supplier_association, supplier WHERE tarrif.tarrif_id=supplier_association.tarrif_id AND supplier_association.supplier_id=supplier.supplier_id");
                    $sql->execute(); // runs SQL statement
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row
                    if ($result != false) {
                        
                        foreach ($rows as $row) {

                            echo "<tr>";
                            echo "<td class='overflow-ellipsis'> " . $row['tarrif_id'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> "  . $row['gas_price'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> "  . $row['gas_standing_charge'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> "  . $row['electricity_price'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> "  . $row['electricity_standing_charge'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['length'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['tarrif_name'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['supplier'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['tcr'] . "</td>";
                            $elecTCR = $row['tcr'] + 0.1000;
                            echo "<td  class='overflow-ellipsis'> " . $elecTCR . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['type'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['cancellation_fee'] . "</td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr>";
                            echo "<td>" . 'Nothing to Display' . "</td>";
                            echo "</tr>";
                    }
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
    </body>
</html>
