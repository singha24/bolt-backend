<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';
if (isset($_REQUEST['verify']) && $_REQUEST['verify'] = "Verify") {
    verify();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Customers</title>
        <link rel="stylesheet" type="text/css" href="../css/view_customers.css">
    </head>
    <body>
        <h1>Customers</h1>
        <hr>
        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">No.</a></th>
                        <th><a href="#">Name</a></th>
                        <th><a href="#">Address</a></th>
                        <th><a href="#">Meter Number</a></th>
                        <th><a href="#">Email</a></th>
                        <th><a href="#">Authorised?</a></th>
                        <th><a href="#">Tarrif</a></th>
                        <th><a href="#">Supplier</a></th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL

                    $sql = $conn->prepare("SELECT customer.*, meter_association.*, tarrif.tarrif_name, tarrif.tarrif_id, supplier_association.*, supplier.supplier FROM customer, meter_association, tarrif, supplier_association, supplier WHERE "
                            . "customer.meter_number=meter_association.meter_number AND "
                            . "meter_association.tarrif_id=tarrif.tarrif_id "
                            . "AND tarrif.tarrif_id=supplier_association.tarrif_id "
                            . "AND supplier_association.supplier_id=supplier.supplier_id");
                    $sql->execute(); // runs SQL statement
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row
                    if ($result != false) {
                        $counter = 1;
                        foreach ($rows as $row) {

                            echo "<tr>";
                            echo "<td>" . $counter . "</td>";
                            echo "<td class='overflow-ellipsis'> " . $row['name'] . ' ' . $row['surname'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> "  . $row['address'] . ' ' . $row['postcode'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['meter_number'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['email'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['authorisation'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['tarrif_name'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['supplier'] . "</td>";
                            echo "</tr>";
                            $counter++;
                        }
                    } else {
                        echo "<tr>";
                            echo "<td>" . 'Nothing to Display' . "</td>";
                            echo "</tr>";
                    }
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
    </body>
</html>
