<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';
if (isset($_REQUEST['verify']) && $_REQUEST['verify'] = "Verify") {
    verify();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Usage</title>
        <link rel="stylesheet" type="text/css" href="../css/view_customers.css">
    </head>
    <body>
        <h1>Usage</h1>
        <hr>
        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">Customer Name</a></th>
                        <th><a href="#">Gas Usage</a></th>
                        <th><a href="#">Electricity Usage</a></th>
                        <th><a href="#">Supplier</a></th>
                        <th><a href="#">Cost per quarter (£)</a></th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL
                    
                    $sql = $conn->prepare("SELECT customer.*, usage_association.*, user_usage.gas_usage, user_usage.electricity_usage, "
                            . "meter_association.*, tarrif.tarrif_id, tarrif.gas_price, tarrif.electricity_price, tarrif.length, supplier_association.*, supplier.* "
                            . "FROM customer, usage_association, user_usage, meter_association, tarrif, supplier_association, supplier "
                            . "WHERE customer.meter_number=usage_association.meter_number AND usage_association.usage_id=user_usage.usage_id AND "
                            . "customer.meter_number=meter_association.meter_number AND meter_association.tarrif_id=tarrif.tarrif_id AND tarrif.tarrif_id=supplier_association.tarrif_id AND "
                            . "supplier_association.supplier_id=supplier.supplier_id");
                    try{
                    $sql->execute(); // runs SQL statement
                    }catch(Exception $e){
                        echo $e->getMessage();
                    }
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row
                    if ($result != false) {
                        
                        foreach ($rows as $row) {

                            echo "<tr>";
                            echo "<td class='overflow-ellipsis'> " . $row['name'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> "  . $row['gas_usage'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['electricity_usage'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['supplier'] . "</td>";
                            $total = (($row['gas_price']*$row['gas_usage']) + ($row['electricity_price']*$row['electricity_usage'])) * 3;
                            echo "<td  class='overflow-ellipsis'> " . $total . "</td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr>";
                            echo "<td>" . 'Nothing to Display' . "</td>";
                            echo "</tr>";
                    }
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
    </body>
</html>
