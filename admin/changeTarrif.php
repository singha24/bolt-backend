<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../DAO/ProcessNewTarrif.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Change Tarrif For User</title>
        <link rel="stylesheet" type="text/css" href="../css/newbook.css">
    </head>
    <body>
        <h1> Change Tarrif </h1>
        <hr>
        <div class="newTarrif">
            <form id="new_tarrif" method="get" action="">
                Meter Number: 
                <input id="meter_number" type="text" name="meter" maxlength="13" required>
                <br/>

                Tarrif:
                <?php
                echo '<select name="tarrif">';
                getTarrifs();
                echo '</select>';
                ?>
                <br/>

                <input type='hidden' name='function' value="meter_number" />
                <input type="submit" value="Submit" id="submitForm" />
            </form>
        </div>

        <hr/>

        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">Name</a></th>
                        <th><a href="#">PostCode</a></th>
                        <th><a href="#">Meter Number</a></th>
                        <th><a href="#">Tarrif</a></th>
                        <th><a href="#">Tarrif ID</a></th>
                        <th><a href="#">Supplier</a></th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($_GET['meter'])) {
                        $conn = getDb(); // gets connection to MySQL
                        $meter_number = $_GET['meter'];

                        $tarrif = $_GET['tarrif'];
                        var_dump($tarrif);

                        $sql = $conn->prepare("UPDATE meter_association SET meter_association.tarrif_id=:tarrif_id WHERE meter_association.meter_number=:meter_number");

                        $sql->bindValue(":tarrif_id", $tarrif);
                        $sql->bindValue(":meter_number", $meter_number);

                        /* $sql = $conn->prepare("SELECT customer.name, customer.surname, customer.address, customer.postcode, meter_association.*, tarrif.tarrif_name, tarrif.tarrif_id, supplier_association.*, supplier.supplier FROM customer, meter_association, tarrif, supplier_association, supplier WHERE "
                          . "customer.meter_number=:meter_number AND "
                          . "customer.meter_number=meter_association.meter_number AND "
                          . "meter_association.tarrif_id=tarrif.tarrif_id "
                          . "AND tarrif.tarrif_id=supplier_association.tarrif_id "
                          . "AND supplier_association.supplier_id=supplier.supplier_id");
                          $sql->bindValue(":meter_number", $meter_number); */
                        $sql->execute(); // runs SQL statement
                        /* $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                          $rows = $sql->fetchAll(); // holds array containing each row
                          if ($result != false) {
                          $counter = 1;
                          foreach ($rows as $row) {

                          echo "<td class='overflow-ellipsis'> " . $row['name'] . ' ' . $row['surname'] . "</td>";
                          echo "<td  class='overflow-ellipsis'> " . $row['postcode'] . "</td>";
                          echo "<td  class='overflow-ellipsis'> " . $row['meter_number'] . "</td>";
                          echo "<td  class='overflow-ellipsis'> " . $row['tarrif_name'] . "</td>";
                          echo "<td  class='overflow-ellipsis'> " . $row['tarrif_id'] . "</td>";
                          echo "<td  class='overflow-ellipsis'> " . $row['supplier'] . "</td>";
                          echo "</tr>";
                          }
                          } else {
                          echo "<tr>";
                          echo "<td>" . 'Nothing to Display' . "</td>";
                          echo "</tr>";
                          } */
                    }
                    ?>

                </tbody>
            </table>

        </div>


    </body>
</html>
