<!DOCTYPE html>
<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
include '../controller/database.php';
if (isset($_REQUEST['verify']) && $_REQUEST['verify'] = "Verify") {
    verify();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Historic Usage</title>
        <link rel="stylesheet" type="text/css" href="../css/view_customers.css">
    </head>
    <body>
        <h1>Usage</h1>
        <hr>
        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">Customer Number</a></th>
                        <th><a href="#">Day</a></th>
                        <th><a href="#">Time</a></th>
                        <th><a href="#">Gas Usage</a></th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL
                    
                    $sql = $conn->prepare("SELECT * FROM historic_usage_elec");
                    try{
                    $sql->execute(); // runs SQL statement
                    }catch(Exception $e){
                        echo $e->getMessage();
                    }
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row
                    if ($result != false) {
                        
                        foreach ($rows as $row) {

                            echo "<tr>";
                            echo "<td class='overflow-ellipsis'> " . $row['meter_number'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> "  . $row['day'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                            echo "<td  class='overflow-ellipsis'> " . $row['usage'] . "</td>";
                            //echo "<td  class='overflow-ellipsis'> " . $row['price'] . "</td>";
                            echo "</tr>";
                        }
                    } else {
                        echo "<tr>";
                            echo "<td>" . 'Nothing to Display' . "</td>";
                            echo "</tr>";
                    }
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
    </body>
</html>
