<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class Customer {
    
    private $params;
    
    public function __construct($params) {
        $this->params = $params;
    }

    function request_customerAction() {
        $todo = new CustomerModel();
        $todo->meter_id = $this->params['meter_id'];
        $todo->getCustomer($this->params['meter_id']);
        
        return $todo;
    }
    
    //total, consumption for this month, and previous month. ELECTRICITY
    function request_homeElecAction(){
        $c = new UsageModel();
        $c->meter_number = $this->params['meter_id'];
        $c->getElecHome();
        
        return $c;
    }
    //TODAY GAS
    function getGasConsumptionAction(){
        $c = new UsageModel();
        $c->meter_number = $this->params['meter_id'];
        $c->start = $this->params['start'];
        $c->getTodayGasConumption();
        
        return $c;
    }
    
    //TODAY GAS
    /*function getGasConsumptionAction(){
        $c = new UsageModel();
        $c->meter_number = $this->params['meter_id'];
        $c->start = $this->params['start'];
        $c->getTodayGasConumption();
        
        return $c;
    }*/
    
    function request_dayAction(){
        $c = new DayModel();
        //$c->meter_number = $this->params['meter_id'];
        $c->day = $this->params['day'];
        $c->getDay();
        
        return $c;
    }
    
    function request_monthAction(){
        $c = new DayModel();
        $c->day = $this->params['day'];
        $c->getMonth();
        return $c;
    }
    
    function request_weekAction(){
        $c = new DayModel();
        $c->day = $this->params['day'];
        $c->getWeek();
        return $c;
    }

}

?>