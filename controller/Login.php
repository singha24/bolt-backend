<?php

if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class Login {
    
    private $params;
    
    public function __construct($params) {
        $this->params = $params;
    }

    function request_loginAction() {
        $todo = new LoginModel();
        $todo->email = $this->params['email'];
        $todo->password = $this->params['password'];
        $todo->authenticate();
        
        return $todo;
    }

}

?>