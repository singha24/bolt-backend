<?php

if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class Tarrif {
    
    private $params;
    
    public function __construct($params) {
        $this->params = $params;
    }

    function request_tarrifAction() {
        $todo = new TarrifModel();
        $todo->getAllTarrifs();
        
        return $todo;
    }
    
    function change_tarrifAction(){
        $todo = new TarrifModel();

        $todo->meter_id = $this->params['meter_id'];
        $todo->tarrif_id = $this->params['tarrif_id'];
        
        $todo->changeTarrif();
        //return the todo item in array format
        return $todo;
    }

}

?>