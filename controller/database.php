<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
function getDb() {
    try {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "bolt";

        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (Exception $e) {
        die("Unable to connect to database");
    }
}

function login() {
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];
    $password = md5($password);
    $conn = getDb();

    $sql = $conn->prepare("SELECT username, password FROM dev WHERE username = :username AND password = :password LIMIT 1");
    $sql->bindValue(":username", $username);
    $sql->bindValue(":password", $password);
    $sql->execute();
    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);

    $rows = $sql->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        //if the query returns a value else return 1. 1 indicates login failure. 0 implies pending approval. 
        foreach ($rows as $row) {
            $_SESSION['session_id'] = $row['username'];
            return true;
        }
    } else {
        echo "<script type='text/javascript'>alert('Failed to login. Please contact the site administrator.');</script>";
    }
}

function logout() {
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
        );
    }
    session_destroy();
    header('Location: index.php');
}

?>