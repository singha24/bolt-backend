<!DOCTYPE html>
<?php
include 'controller/database.php';
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_REQUEST['login']) && $_REQUEST['login'] = "login") {
    login();
}
if (isset($_REQUEST['logout']) && $_REQUEST['logout'] = "logout") {
    logout();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div class="login">
            <?php
            if ($_SESSION == NULL) {
                echo '<div class="login/register">';

                echo '<div class="login">';
                echo '<form id="login_form" method="post">';
                echo '<input type="text" autofocus="true" name="username" placeholder="username" id="username">';
                echo '<input type="password" name="password" placeholder="Password" id="password">';
                echo '<input type="hidden" name="login" value="login" />';
                echo '<button id="login_submit" type="submit">Sign in</button>';
                echo '</form>';
                echo '</div>';
                echo '<br/>';
                echo '</div>';
            } else {
                echo '<form id="logout" method="post">';
                echo '<input type="hidden" name="logout" value="logout" />';
                echo '<button id="logout_submit" type="submit">Logout</button>';
                echo '</form>';
            }
            ?>
        </div>
        <?php
        if (isset($_SESSION['session_id'])) {
            echo'<ol>';
            echo'<li><a href = "admin/NewCustomer.php">New Customer</a></li>';
            echo'<li><a href = "admin/ViewAllCustomers.php">View Customer</a></li>';
            echo'<li><a href = "admin/Tarrifs.php">View Tarrifs</a></li>';
            echo'<li><a href = "admin/ViewUsage.php">View Usage</a></li>';
            echo'<li><a href = "admin/historic_usage.php">View Historic Usage</a></li>';
            echo'<li><a href = "admin/changeTarrif.php">Change Tarrif</a></li>';
            echo'<li><a href = "admin/ElectricityUsage.php">Electricity Usage</a></li>';
            echo'<li><a href = "admin/GasUsage.php">Gas Usage</a></li>';
            echo'<li><a href = "admin/One Day Usage.php">One Day Usage ELEC</a></li>';
            echo'<li> <a href = "admin/OneDayUsageGAS.php">One Day Usage GAS</a></li>';
            echo'<li><a href = "admin/EveryHalfHour.php">Every Half Hour</a></li>';
            echo'</ol>';
            
            echo'<button type="button"><a href="controller/Push.php">SEND PUSH NOTIFICATION</a></button>';
        }
        ?>
    </body>
</html>
