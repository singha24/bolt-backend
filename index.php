<?php
header('Content-Type: application/json');

include_once 'model/TarrifModel.php';
include_once 'model/CustomerModel.php';
require 'controller/database.php';
include_once 'model/UsageModel.php';
include_once 'model/LoginModel.php';
include_once 'model/DayModel.php';

try {
    //get all of the parameters in the POST/GET request
    $params = $_REQUEST;

    //var_dump($params);
    //get the controller and format it correctly so the first
    //letter is always capitalized
    $controller = ucfirst(strtolower($params['controller']));

    //get the action and format it correctly so all the
    //letters are not capitalized, and append 'Action'
    $action = strtolower($params['action']) . 'Action';

    //check if the controller exists. if not, throw an exception
    if (file_exists("controller/{$controller}.php")) {
        include_once "controller/{$controller}.php";
    } else {
        header('Location: front-end-index.php');
        throw new Exception('Controller is invalid.');
    }

    //create a new instance of the controller, and pass
    //it the parameters from the request
    $controller = new $controller($params);

    //check if the action exists in the controller. if not, throw an exception.
    if (method_exists($controller, $action) === false) {
        throw new Exception('Action is invalid.');
    }

    //execute the action
    $result['data'] = $controller->$action();
    //$result['success'] = true;
} catch (Exception $e) {
    //catch any exceptions and report the problem
    $result = array();
    $result['errormsg'] = $e->getMessage();
}
echo json_encode($result);
exit();

