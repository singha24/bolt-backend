<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class CustomerModel {

    public $meter_id;
    public $name;
    public $supplier;
    public $tarrif;
    //public $expense;
    

    public function getCustomer() {
        $conn = getDb(); // gets connection to MySQL

        $sql = $conn->prepare("SELECT customer.name, customer.surname, customer.address, customer.postcode, meter_association.*, tarrif.tarrif_name, tarrif.tarrif_id, supplier_association.*, supplier.supplier FROM customer, meter_association, tarrif, supplier_association, supplier WHERE "
                . "customer.meter_number=:meter_number AND "
                . "customer.meter_number=meter_association.meter_number AND "
                . "meter_association.tarrif_id=tarrif.tarrif_id "
                . "AND tarrif.tarrif_id=supplier_association.tarrif_id "
                . "AND supplier_association.supplier_id=supplier.supplier_id");
        $sql->bindValue(":meter_number", $this->meter_id);
        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $result = $sql->fetchAll(); // holds array containing each row

        $this->name = $result[0]['name'] . " " . $result[0]['surname'];
        $this->supplier = $result[0]['supplier'];
        $this->tarrif = $result[0]['tarrif_name'];
        
        return $result;
    }

    public function toArray() {
        //return an array version of the todo item
        return array(
            'meter_id' => $this->meter_id
        );
    }

}
