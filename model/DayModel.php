<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class DayModel {

    public $day;
    public $time;
    public $gas_usage;
    public $elec_usage;
    public $total_gas_usage;
    public $total_elec_usage;

    public function getDay() {
        $conn = getDb();

        $start = $this->day;
        $date = new DateTime($start);
        $requestDate = $date->format('Y-m-d');
        //date_default_timezone_set("Europe/London");
        //$time = date("H:i");

        $sql = $conn->prepare("SELECT * FROM historic_usage_gas WHERE day = :start");

        //$sql->bindValue(":meter_number", $meter_number);
        $sql->bindValue(":start", $requestDate);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        //var_dump($rows);

        $total_gas_usage = 0;
        $i = 0;
        if ($result != false) {
            foreach ($rows as $row) {
                $this->gas_usage[$i]['date'] = $row['day'];
                $this->gas_usage[$i]['time'] = $row['time'];
                $this->gas_usage[$i]['gas_usage'] = $row['gas_usage'];
                $total_gas_usage += $row['gas_usage'];
                $i++;
            }
            $this->total_gas_usage = $total_gas_usage;
            $this->getElecUsageForDay();
        }
    }

    public function getElecUsageForDay() {
        $conn = getDb(); // gets connection to MySQL

        $start = $this->day;
        $date = new DateTime($start);
        $requestDate = $date->format('Y-m-d');

        $sql = $conn->prepare("SELECT * FROM historic_usage_elec WHERE day =:start");

        //$sql->bindValue(":meter_number", $meter_number);
        $sql->bindValue(":start", $requestDate);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row


        if ($result != false) {
            $total_elec_usage = 0;
            $i = 0;
            foreach ($rows as $row) {
                $this->elec_usage[$i]['elec_usage'] = $row['elec_usage'];
                $this->elec_usage[$i]['date'] = $row['day'];
                $this->elec_usage[$i]['time'] = $row['time'];
                $total_elec_usage += $row['elec_usage'];
                $i++;
            }

            $this->total_elec_usage = $total_elec_usage;
        }
    }

    public function getMonth() {
        $conn = getDb(); // gets connection to MySQL

        $start = $this->day;
        $date = new DateTime($start);
        $requestDate = $date->format('Y-m-d');

        $previous = new DateTime($requestDate);
        $previous->modify('+ 1 month');
        $endOfMonth = $previous->format('Y-m-d');
        $temp = new DateTime($endOfMonth);
        $temp->modify('-1 day');
        $endOfMonth = $temp->format('Y-m-d');

        //echo '<pre>';
        //echo $endOfMonth . ' ' . $requestDate;
        //echo '</pre>';

        $sql = $conn->prepare("SELECT historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage FROM "
                . "historic_usage_elec WHERE "
                . "historic_usage_elec.day BETWEEN :start AND :end");

        $sql->bindValue(":start", $requestDate);
        $sql->bindValue(":end", $endOfMonth);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        //var_dump($rows);
        $total_elec_usage = 0;
        $i = 0;
        $counter = 1;
        $endOfDayUsage = 0;
        foreach ($rows as $row) {
            $endOfDayUsage += $row['elec_usage'];

            if ($counter == 48) {
                $this->elec_usage[$i]['endOfDayUsageElec'] = $endOfDayUsage;
                $counter = 0;
                $endOfDayUsage = 0;
            }
            $total_elec_usage += $row['elec_usage'];
            $counter++;
            $i++;
        }
        $this->total_elec_usage = $total_elec_usage;

        $this->getMonthGas();
    }

    public function getMonthGas() {
        $conn = getDb(); // gets connection to MySQL

        $start = $this->day;
        $date = new DateTime($start);
        $requestDate = $date->format('Y-m-d');

        $previous = new DateTime($requestDate);
        $previous->modify('+ 1 month');
        $endOfMonth = $previous->format('Y-m-d');
        $temp = new DateTime($endOfMonth);
        $temp->modify('-1 day');
        $endOfMonth = $temp->format('Y-m-d');
        //echo '<pre>';
        //echo $prevousMonth . ' ' . $requestDate;
        //echo '</pre>';

        $sql = $conn->prepare("SELECT historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage FROM "
                . "historic_usage_gas WHERE "
                . "historic_usage_gas.day BETWEEN :start AND :end");

        $sql->bindValue(":start", $requestDate);
        $sql->bindValue(":end", $endOfMonth);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        //var_dump($rows);
        $total_gas_usage = 0;
        $i = 0;
        $counter = 1;
        $endOfDayUsage = 0;
        foreach ($rows as $row) {

            $endOfDayUsage += $row['gas_usage'];

            if ($counter % 48 == 0) {
                $this->gas_usage[$i]['endOfDayUsageGas'] = $endOfDayUsage;
                $endOfDayUsage = 0;
                $counter = 0;
            }
            $total_gas_usage += $row['gas_usage']; //total

            $counter++;
            $i++;
        }
        $this->total_gas_usage = $total_gas_usage;
    }

    public function getWeek() {
        $conn = getDb(); // gets connection to MySQL

        $start = $this->day;
        $date = new DateTime($start);
        $requestDate = $date->format('Y-m-d');

        $previous = new DateTime($requestDate);
        $previous->modify('-7 days');
        $prevousWeek = $previous->format('Y-m-d');
        //echo '<pre>';
        //echo $prevousWeek . ' ' . $requestDate;
        //echo '</pre>';

        $sql = $conn->prepare("SELECT historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage FROM "
                . "historic_usage_gas WHERE "
                . "historic_usage_gas.day BETWEEN :start AND :end");

        $sql->bindValue(":start", $prevousWeek);
        $sql->bindValue(":end", $requestDate);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        //var_dump($rows);
        $total_gas_usage = 0;
        $i = 0;
        $counter = 1;
        $endOfDayUsage = 0;
        $dayNo = 1;
        foreach ($rows as $row) {
            //echo $row['elec_usage'];
            $endOfDayUsage += $row['gas_usage'];

            if ($counter == 48) {
                $this->gas_usage[$i]['day'] = $row['day'];
                $this->gas_usage[$i]['gas_usage'] = $row['gas_usage'];
                $counter = 0;
            }

            $total_gas_usage += $row['gas_usage'];
            
            $dayNo++;
            $counter++;
            $i++;
        }
        $this->total_gas_usage = $total_gas_usage;
        
        $this->getWeekElec();
    }
    
    public function getWeekElec() {
        $conn = getDb(); // gets connection to MySQL

        $start = $this->day;
        $date = new DateTime($start);
        $requestDate = $date->format('Y-m-d');

        $previous = new DateTime($requestDate);
        $previous->modify('-7 days');
        $prevousWeek = $previous->format('Y-m-d');
        //echo '<pre>';
        //echo $prevousWeek . ' ' . $start;
        //echo '</pre>';

        $sql = $conn->prepare("SELECT historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage FROM "
                . "historic_usage_elec WHERE "
                . "historic_usage_elec.day BETWEEN :start AND :end");

        $sql->bindValue(":start", $prevousWeek);
        $sql->bindValue(":end", $requestDate);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        //var_dump($rows);
        $total_elec_usage = 0;
        $i = 0;
        $counter = 1;
        $endOfDayUsage = 0;
        $dayNo = 1;
        foreach ($rows as $row) {
            //echo $row['elec_usage'];
            $endOfDayUsage += $row['elec_usage'];

            if ($counter == 48) {
                $this->elec_usage[$i]['day'] = $row['day'];
                $this->elec_usage[$i]['elec_usage'] = $row['elec_usage'];
                $counter = 0;
            }

            $total_elec_usage += $row['elec_usage'];
            
            $dayNo++;
            $counter++;
            $i++;
        }
        $this->total_elec_usage = $total_elec_usage;
    }

}
