<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class LoginModel {

    public $email;
    public $password;
    public $loggedIn;
    public $meter_number;

    public function authenticate() {
        $conn = getDb(); // gets connection to MySQL

        
        
        $sql = $conn->prepare("SELECT customer.email, customer.password, customer.meter_number FROM customer WHERE "
                . "customer.email=:email AND "
                . "customer.password=:password");
        $sql->bindValue(":email", $this->email);
        $sql->bindValue(":password", md5($this->password));
        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $result = $sql->fetchAll(); // holds array containing each row

        if ($result != null) {
            $this->meter_number = $result[0]['meter_number'];
            $this->loggedIn = true;
            $this->password = "correct";
        }else{
            $this->meter_number = null;
            $this->password = "incorrect";
            $this->loggedIn = false;
        }
        
        return $result;
    }

}
