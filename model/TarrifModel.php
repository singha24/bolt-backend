<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class TarrifModel {

    public $gasPrice;
    public $elecPrice;
    public $length;
    public $tarrifName;
    public $tcr;
    public $gasStandingCharge;
    public $electricityStandingCharge;
    public $cancellationFee;
    public $meter_id;
    public $tarrif_id;

    public function getAllTarrifs() {
        $conn = getDb(); // gets connection to MySQL

        $sql = $conn->prepare("SELECT * FROM tarrif");
        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        foreach ($rows as $row) {
            $this->gasPrice[] = $row['gas_price'];
            $this->elecPrice[] = $row['electricity_price'];
            $this->length[] = $row['length'];
            $this->tarrifName[] = $row['tarrif_name'];
            $this->tcr[] = $row['tcr'];
            $this->gasStandingCharge[] = $row['gas_standing_charge'];
            $this->electricityStandingCharge[] = $row['electricity_standing_charge'];
            $this->cancellationFee[] = $row['cancellation_fee'];
        }


        return $rows;
    }

    public function changeTarrif() {
        $conn = getDb(); // gets connection to MySQL

        $sql = $conn->prepare("UPDATE meter_association SET meter_association.tarrif_id=:tarrif_id WHERE meter_association.meter_number=:meter_number");

        $sql->bindValue(":tarrif_id", $this->tarrif_id);
        $sql->bindValue(":meter_number", $this->meter_id);

        $sql->execute();
        
        
        $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, tarrif.*, supplier_association.*, supplier.supplier FROM customer, meter_association, tarrif, supplier_association, supplier WHERE "
                . "customer.meter_number=:meter_number AND "
                . "customer.meter_number=meter_association.meter_number AND "
                . "meter_association.tarrif_id=tarrif.tarrif_id "
                . "AND tarrif.tarrif_id=supplier_association.tarrif_id "
                . "AND supplier_association.supplier_id=supplier.supplier_id");
        $sql->bindValue(":meter_number", $this->meter_id);
        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        foreach ($rows as $row) {
            $this->gasPrice[] = $row['gas_price'];
            $this->elecPrice[] = $row['electricity_price'];
            $this->length[] = $row['length'];
            $this->tarrifName[] = $row['tarrif_name'];
            $this->tcr[] = $row['tcr'];
            $this->gasStandingCharge[] = $row['gas_standing_charge'];
            $this->electricityStandingCharge[] = $row['electricity_standing_charge'];
            $this->cancellationFee[] = $row['cancellation_fee'];
        }

        return $rows;
        
    }

}
