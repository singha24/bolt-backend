<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class TodoItem {

    public $meter_id;
    public $test;

    public function save() {

        //get the array version of this todo item
        $todo_item_array = $this->toArray();
        
        //return the array version
        return $todo_item_array;
    }

    public function toArray() {
        //return an array version of the todo item
        return array(
            'meter_id' => $this->meter_id
        );
    }

}
