<?php
if (!(isset($_SESSION['session_id']))) {
    header('Location: ../front-end-index.php');
}
class UsageModel {

    public $meter_number;
    public $start;
    //public $end;
    public $date;
    //public $time;
    public $elec_consumption;
    public $gas_consumption;
    public $elec_total;
    public $gas_total;
    public $total_elec_usage;
    public $total_elec_previous;
    public $total_gas_usage;
    public $total_gas_previous;

    public function getRange() {

        /* $i = 0;
          $start = '"' . $this->start . '"';
          $end = '"' . $this->end . '"';
          $conn = getDb(); // gets connection to MySQL
          $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage, tarrif.* FROM "
          . "customer, meter_association, historic_usage_elec, tarrif WHERE "
          . "customer.meter_number=:meter_number AND "
          . "meter_association.meter_number=customer.meter_number AND "
          . "meter_association.tarrif_id=tarrif.tarrif_id AND "
          . "meter_association.meter_number=historic_usage_elec.meter_number AND "
          . "historic_usage_elec.day BETWEEN :start AND :end");
          $sql->bindValue(":start", $this->start);
          $sql->bindValue(":end", $this->end);
          $sql->bindValue(":meter_number", $this->meter_number);
          $sql->execute(); // runs SQL statement
          // set the resulting array to associative
          $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
          $rows = $sql->fetchAll(); // holds array containing each row
          foreach ($rows as $row) {
          $this->day[$i] = $row['day'];
          $this->time[$i] = $row['time'];
          $this->usage[$i] = $row['usage'];
          $i++;
          }


          return $rows; */
    }

    function getStartOfMonth($today) {
        $dd = DateTime::createFromFormat("Y-m-d", $today);
        $todaysDay = $dd->format("d");

        $startOfCurrentMonthTemp = ($todaysDay - $todaysDay) + 1; ///start of the month A

        $mm = DateTime::createFromFormat("Y-m-d", $today);
        $todaysMonth = $mm->format("m");


        $yy = DateTime::createFromFormat("Y-m-d", $today);
        $todaysYear = $yy->format("y");

        $startOfCurrentMonthT = $todaysYear . '-' . $todaysMonth . '-' . $startOfCurrentMonthTemp;

        $date = new DateTime($startOfCurrentMonthT);
        $result = $date->format('Y-m-d');

        return $result;
    }

    function getTodaysDateMonthMinus1($today) {

        $dd = DateTime::createFromFormat("Y-m-d", $today);
        $todaysDay = $dd->format("d");

        $todaysDayMinus1 = $todaysDay - 1; //temp

        $mm = DateTime::createFromFormat("Y-m-d", $today);
        $todaysMonth = $mm->format("m");

        $yy = DateTime::createFromFormat("Y-m-d", $today);
        $todaysYear = $yy->format("y");

        $todaysDateMinus1Temp = $todaysYear . '-' . $todaysMonth . '-' . $todaysDayMinus1; //Todays Date Minus 1 day

        $date = new DateTime($todaysDateMinus1Temp);
        $result = $date->format('Y-m-d');

        return $result;
    }

    function getStartOfPreviousMonth($today) {

        $previous = strtotime($today . ' - 1 month');

        $startOfPreviousMonthTemp = gmdate("Y-m-d", $previous); //previous month

        $d = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp); //Getting first of previous month
        $startDay = $d->format("d");
        $day = ($startDay - $startDay) + 1;

        $m = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp);
        $month = $m->format("m");

        $y = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp);
        $year = $y->format("y");

        $startOfPreviousMonthTemp = $year . '-' . $month . '-' . $day;

        $date = new DateTime($startOfPreviousMonthTemp);
        $result = $date->format('Y-m-d');

        return $result;
    }

    public function getElecHome() {
        date_default_timezone_set("Europe/London");

        $conn = getDb(); // gets connection to MySQL

        $meter_number = $this->meter_number;
        $this->start = date("Y-m-d");
        $today = $this->start; //today

        $todaysDateMinus1 = $this->getTodaysDateMonthMinus1($today); //todays date -1
        $startOfCurrentMonth = $this->getStartOfMonth($today); //start of month
        //echo $today . '     ' . $startOfCurrentMonth;

        $time = date("H:i");

        // get all consumption data from the start of current month until yesterday

        $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage, tarrif.* FROM "
                . "customer, meter_association, historic_usage_elec, tarrif WHERE "
                . "customer.meter_number=:meter_number AND "
                . "meter_association.meter_number=customer.meter_number AND "
                . "meter_association.tarrif_id=tarrif.tarrif_id AND "
                . "meter_association.meter_number=historic_usage_elec.meter_number AND "
                . "historic_usage_elec.day BETWEEN :start AND :end");

        $sql->bindValue(":meter_number", $meter_number);
        $sql->bindValue(":start", $startOfCurrentMonth); //curent Date
        $sql->bindValue(":end", $todaysDateMinus1); //start of current Month

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row

        $elec_usage = 0;
        $elec_sc = 0;
        $elec_price = 0;

        foreach ($rows as $row) {
            $elec_usage += $row['elec_usage'];
            $elec_sc = $row['electricity_standing_charge'];
            $elec_price = $row['electricity_price'];
        }
        //echo '<hr/>';
        // get all consumption data for today's day until te time

        $sql2 = $conn->prepare("SELECT historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage FROM "
                . "historic_usage_elec WHERE "
                . "historic_usage_elec.day = :endP AND "
                . "historic_usage_elec.time <= :time");
        $sql2->bindValue(":time", $time);
        $sql2->bindValue(":endP", $today);
        $sql2->execute();
        $result = $sql2->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql2->fetchAll(); // holds array containing each row

        foreach ($rows as $row) {
            $elec_usage += $row['elec_usage'];
        }

        // add result from sql + result from sql2
        $diff = abs(strtotime($startOfCurrentMonth) - strtotime($today));

        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24)) + 1;

        //echo '<pre>';
        //printf("%d years, %d months, %d days\n", $years, $months, $days);
        //echo '</pre>';



        $total = ($days * $elec_sc) + ($elec_usage * $elec_price); //1

        $this->elec_total = $total;
        $this->total_elec_usage = $elec_usage;



        //Gets data for previous month
        $previous = strtotime($today . ' - 1 month');

        $startOfPreviousMonthTemp = gmdate("Y-m-d", $previous); //previous month

        $d = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp); //Getting first of previous month
        $startDay = $d->format("d");
        $endDay = $d->format("d");
        $day = ($endDay - $endDay) + 1;

        $m = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp);
        $month = $m->format("m");

        $y = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp);
        $year = $y->format("y");



        $startOfPreviousMonthTemp = $year . '-' . $month . '-' . $day;
        $date = new DateTime($startOfPreviousMonthTemp);
        $startOfPreviousMonth = $date->format('Y-m-d');



        $actualEndOfPreviousMonthTemp = $year . '-' . $month . '-' . $endDay;
        $date = new DateTime($actualEndOfPreviousMonthTemp);
        $actualEndOfPreviousMonth = $date->format('Y-m-d');

        $endDay = $endDay - 1;
        $actualEndOfPreviousMonthMinus1Temp = $year . '-' . $month . '-' . $endDay;
        $date = new DateTime($actualEndOfPreviousMonthMinus1Temp);
        $actualEndOfPreviousMonthMinus1 = $date->format('Y-m-d');

        $usagePreviousMonth = 0;

        $sql3 = $conn->prepare("SELECT * FROM historic_usage_elec WHERE "
                . "historic_usage_elec.meter_number=:meter_number AND "
                . "historic_usage_elec.day BETWEEN :start AND :end");

        $sql3->bindValue(":meter_number", $meter_number);
        $sql3->bindValue(":start", $startOfPreviousMonth);
        $sql3->bindValue(":end", $actualEndOfPreviousMonthMinus1);

        $sql3->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql3->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql3->fetchAll(); // holds array containing each row

        foreach ($rows as $row) {
            $usagePreviousMonth += $row['elec_usage'];
        }

        //gets time and usage for previous month
        $sql4 = $conn->prepare("SELECT historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage FROM "
                . "historic_usage_elec WHERE "
                . "historic_usage_elec.day = :endP AND "
                . "historic_usage_elec.time <= :time");

        $sql4->bindValue(":time", $time);
        $sql4->bindValue(":endP", $actualEndOfPreviousMonth);
        $sql4->execute();
        $result = $sql4->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql4->fetchAll(); // holds array containing each row
        foreach ($rows as $row) {
            $usagePreviousMonth += $row['elec_usage'];
        }


        //calculations
        $this->total_elec_previous = $usagePreviousMonth;

        $this->getGasHome();
    }

    public function getGasHome() {

        $conn = getDb(); // gets connection to MySQL

        $meter_number = $this->meter_number;
        $this->start = date("Y-m-d");
        $today = $this->start;

        $todaysDateMinus1 = $this->getTodaysDateMonthMinus1($today);
        $startOfCurrentMonth = $this->getStartOfMonth($today);


        date_default_timezone_set("Europe/London");
        $time = date("H:i");

        //echo $time;
        //echo '<h1>Electricity Usage From: ' . $start . ' To: ' . $endP1 . '</h1>';
        //echo '<hr>';

        $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage, tarrif.* FROM "
                . "customer, meter_association, historic_usage_gas, tarrif WHERE "
                . "customer.meter_number=:meter_number AND "
                . "meter_association.meter_number=customer.meter_number AND "
                . "meter_association.tarrif_id=tarrif.tarrif_id AND "
                . "meter_association.meter_number=historic_usage_gas.meter_number AND "
                . "historic_usage_gas.day BETWEEN :start AND :end");

        $sql->bindValue(":meter_number", $meter_number);
        $sql->bindValue(":start", $startOfCurrentMonth);
        $sql->bindValue(":end", $todaysDateMinus1);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row

        if ($result != false) {
            $counter = 1;
            $gas_usage = 0;
            $elec_sc = 0;
            $elec_price = 0;
            foreach ($rows as $row) {
                $gas_usage += $row['gas_usage'];
                $elec_sc = $row['gas_standing_charge'];
                $elec_price = $row['gas_price'];
            }
            //echo '<hr/>';

            $sql2 = $conn->prepare("SELECT historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage FROM "
                    . "historic_usage_gas WHERE "
                    . "historic_usage_gas.day = :endP AND "
                    . "historic_usage_gas.time <= :time");
            $sql2->bindValue(":time", $time);
            $sql2->bindValue(":endP", $today);
            $sql2->execute();
            $result = $sql2->setFetchMode(PDO::FETCH_ASSOC);
            $rows = $sql2->fetchAll(); // holds array containing each row

            foreach ($rows as $row) {
                $gas_usage += $row['gas_usage'];
            }
            ///////////////////////////////////////////////////////////////////////
            //Gets data for previous month
            $previous = strtotime($today . ' - 1 month');

            $startOfPreviousMonthTemp = gmdate("Y-m-d", $previous); //previous month

            $d = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp); //Getting first of previous month
            $startDay = $d->format("d");
            $endDay = $d->format("d");
            $day = ($endDay - $endDay) + 1;

            $m = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp);
            $month = $m->format("m");

            $y = DateTime::createFromFormat("Y-m-d", $startOfPreviousMonthTemp);
            $year = $y->format("y");



            $startOfPreviousMonthTemp = $year . '-' . $month . '-' . $day;
            $date = new DateTime($startOfPreviousMonthTemp);
            $startOfPreviousMonth = $date->format('Y-m-d');



            $actualEndOfPreviousMonthTemp = $year . '-' . $month . '-' . $endDay;
            $date = new DateTime($actualEndOfPreviousMonthTemp);
            $actualEndOfPreviousMonth = $date->format('Y-m-d');

            $endDay = $endDay - 1;
            $actualEndOfPreviousMonthMinus1Temp = $year . '-' . $month . '-' . $endDay;
            $date = new DateTime($actualEndOfPreviousMonthMinus1Temp);
            $actualEndOfPreviousMonthMinus1 = $date->format('Y-m-d');
            /////////////////////////////////////////////////////////////

            $usagePreviousMonth = 0;

            $sql3 = $conn->prepare("SELECT * FROM historic_usage_gas WHERE "
                    . "historic_usage_gas.meter_number=:meter_number AND "
                    . "historic_usage_gas.day BETWEEN :start AND :end");

            $sql3->bindValue(":meter_number", $meter_number);
            $sql3->bindValue(":start", $startOfPreviousMonth);
            $sql3->bindValue(":end", $actualEndOfPreviousMonthMinus1);

            $sql3->execute(); // runs SQL statement
            // set the resulting array to associative
            $result = $sql3->setFetchMode(PDO::FETCH_ASSOC);
            $rows = $sql3->fetchAll(); // holds array containing each row

            foreach ($rows as $row) {
                ;
                $usagePreviousMonth += $row['gas_usage'];
            }


            //gets time and usage for previous month
            $sql4 = $conn->prepare("SELECT historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage FROM "
                    . "historic_usage_gas WHERE "
                    . "historic_usage_gas.day = :endP AND "
                    . "historic_usage_gas.time <= :time");

            $sql4->bindValue(":time", $time);
            $sql4->bindValue(":endP", $actualEndOfPreviousMonth);
            $sql4->execute();
            $result = $sql4->setFetchMode(PDO::FETCH_ASSOC);
            $rows = $sql4->fetchAll(); // holds array containing each row
            foreach ($rows as $row) {
                $usagePreviousMonth += $row['gas_usage'];
            }

            $diff = abs(strtotime($startOfPreviousMonth) - strtotime($actualEndOfPreviousMonth));

            $years = floor($diff / (365 * 60 * 60 * 24));
            $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
            $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24)) + 1;
            //printf("%d years, %d months, %d days\n", $years, $months, $days);


            $total = ($days * $elec_sc) + ($gas_usage * $elec_price); //1
            $this->gas_total += $total;
            $this->total_gas_usage = $gas_usage;
            $this->total_gas_previous = $usagePreviousMonth;

            //echo 'Total: £' . $total . ' ';
            //echo 'Elec Usage This Month: ' . $elec_usage . 'kWh || ';
            //echo 'Usage Previous Month ' . $usagePreviousMonth . 'kWh ';
            //echo '<pre/>';
            $this->getTodayGasConumption();
        }
    }

    //Today GAS
    function getTodayGasConumption() {
        $conn = getDb(); // gets connection to MySQL

        $meter_number = $this->meter_number;
        $start = $this->start;

        date_default_timezone_set("Europe/London");
        $time = date("H:i");
        //echo $time;
        //echo '<h1>Electricity Usage: ' . $start . '</h1>';
        //echo '<hr>';

        $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage FROM "
                . "customer, meter_association, historic_usage_gas WHERE "
                . "customer.meter_number=:meter_number AND "
                . "meter_association.meter_number=customer.meter_number AND "
                . "meter_association.meter_number=historic_usage_gas.meter_number AND "
                . "historic_usage_gas.day=:start AND "
                . "historic_usage_gas.time <= :time");

        $sql->bindValue(":meter_number", $meter_number);
        $sql->bindValue(":start", $start);
        $sql->bindValue(":time", $time);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row

        $i = 0;
        if ($result != false) {
            foreach ($rows as $row) {

                $this->elec_consumption[$i]['time'] = $row['time'];
                $this->elec_consumption[$i]['usage'] = $row['gas_usage'];
                $this->elec_consumption[$i]['date'] = $row['day'];
                //array_merge($this->time, $this->time);
                $i++;
            }
            //echo '<hr/>';
            $this->getTodayConumption();
        }
    }

    ////ELECTRICITY
    function getTodayConumption() {
        $conn = getDb(); // gets connection to MySQL

        $meter_number = $this->meter_number;
        $start = $this->start;

        date_default_timezone_set("Europe/London");
        $time = date("H:i");
        //echo $time;
        //echo '<h1>Electricity Usage: ' . $start . '</h1>';
        //echo '<hr>';

        $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_elec.day, historic_usage_elec.time, historic_usage_elec.elec_usage FROM "
                . "customer, meter_association, historic_usage_elec WHERE "
                . "customer.meter_number=:meter_number AND "
                . "meter_association.meter_number=customer.meter_number AND "
                . "meter_association.meter_number=historic_usage_elec.meter_number AND "
                . "historic_usage_elec.day=:start AND "
                . "historic_usage_elec.time <= :time");

        $sql->bindValue(":meter_number", $meter_number);
        $sql->bindValue(":start", $start);
        $sql->bindValue(":time", $time);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row

        $i = 0;
        if ($result != false) {
            foreach ($rows as $row) {
                $this->elec_consumption[$i]['time'] = $row['time'];
                $this->elec_consumption[$i]['usage'] = $row['elec_usage'];
                //array_merge($this->time, $this->time);
                $i++;
            }
            //echo '<hr/>';
            $this->getTodayGasConsumption();
        }
    }

    public function getTodayGasConsumption() {

        $conn = getDb(); // gets connection to MySQL

        $meter_number = $this->meter_number;
        $start = $this->start;

        date_default_timezone_set("Europe/London");
        $time = date("H:i");
        //echo $time;
        //echo '<h1>Electricity Usage: ' . $start . '</h1>';
        //echo '<hr>';

        $sql = $conn->prepare("SELECT customer.meter_number, meter_association.*, historic_usage_gas.day, historic_usage_gas.time, historic_usage_gas.gas_usage FROM "
                . "customer, meter_association, historic_usage_gas WHERE "
                . "customer.meter_number=:meter_number AND "
                . "meter_association.meter_number=customer.meter_number AND "
                . "meter_association.meter_number=historic_usage_gas.meter_number AND "
                . "historic_usage_gas.day=:start AND "
                . "historic_usage_gas.time <= :time");

        $sql->bindValue(":meter_number", $meter_number);
        $sql->bindValue(":start", $start);
        $sql->bindValue(":time", $time);

        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row

        $i = 0;
        if ($result != false) {
            foreach ($rows as $row) {

                //echo "<tr>";
                //echo "<td  class='overflow-ellipsis'> " . $row['meter_number'] . "</td>";
                //echo "<td  class='overflow-ellipsis'> " . $row['day'] . "</td>";
                ///echo "<td  class='overflow-ellipsis'> " . $row['time'] . "</td>";
                //echo "<td  class='overflow-ellipsis'> " . $row['elec_usage'] . "</td>";
                //echo "</tr>";

                $this->gas_consumption[$i]['time'] = $row['time'];
                $this->gas_consumption[$i]['usage'] = $row['gas_usage'];
                $this->gas_consumption[$i]['date'] = $row['day'];
                //array_merge($this->time, $this->time);
                $i++;
            }
            //echo '<hr/>';
        }
    }

}
